@extends('layout.master')

@section('judul')
    <h1>Cast List</h1>
@endsection

@section('content')
<a href="/cast/create" class="btn btn-primary btn-sm my-3">Create</a>

<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">nama</th>
        <th scope="col">umur</th>
        <th scope="col">bio</th>
      </tr>
    </thead>
    <tbody>
    @forelse ($cast as $key => $list)
    <tr>
        <th scope="row">{{$key + 1}}</th>
        <td>{{$list->nama}}</td>
        <td>{{$list->umur}}</td>
        <td>{{$list->bio}}</td>
        <td>
    
            <form action="/cast/{{$list->id}}" method="post">
            @method('delete')
            @csrf
            <a href="/cast/{{$list->id}}" class="btn btn-info btn-sm">Detail</a>
            <a href="/cast/{{$list->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
            <input type="submit" class="btn btn-danger btn-sm" value="Delete">

            </form>
        </td>
      </tr>
    @empty
        <h1>List Empty Data Cast</h1>
    @endforelse 
     
    </tbody>
  </table>
@endsection