@extends('layout.master')

@section('judul')
    <h1>Create Cast</h1>
@endsection

@section('content')
<form action="/cast/{{$cast->id}}" method="post">
    @csrf
    @method('put')
    <div class="mb-3">
      <label class="form-label">Nama</label>
      <input type="text" name="nama" value="{{$cast->nama}}" class="form-control @error('nama') is-invalid @enderror" >
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
<div class="mb-3">
    <label class="form-label">Umur</label>
    <input type="number" name="umur" class="form-control @error('umur') is-invalid @enderror" >
  </div>
  @error('umur')
  <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <div class="mb-3">
      <label class="form-label">Bio</label>
      <textarea name="bio" class="form-control @error("bio") is-invalid @enderror" cols="30" rows="10">{{$cast->bio}}</textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection