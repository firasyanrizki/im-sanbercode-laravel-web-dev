<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\daftarController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [HomeController::class, 'utama']);

Route::get('/register', [daftarController::class, 'register']);

Route::post('/welcome', [daftarController::class, 'welcome']);

Route::get('/data-table', function(){
    return view('datatable');
});

Route::get('/table', function(){
    return view('table');
});

// Route::get('/master', function(){
//     return view ('layout.master');
// });

//CRUDD
//Route yang mengarah ke Cast
Route::get('/cast/create', [CastController::class, 'create']);

//Route untuk inser data inputan ke database table cast
Route::post('cast', [CastController::class, 'store']);

// R -> Read Data
// Route untuk menampikan semua pada table cast di DB
Route::get('/cast', [CastController::class, 'index']);
Route::get('/cast/{id}', [CastController::class, 'show']);

// U -> Update Data
// Route yang mengaraha ke form edit data dengan params id
Route::get('/cast/{id}/edit', [CastController::class, 'edit']);
Route::put('/cast/{id}', [CastController::class, 'update']);

// D => Delete Data
Route::delete('/cast/{id}', [CastController::class, 'destroy']);