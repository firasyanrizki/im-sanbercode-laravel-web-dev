{{-- @extends('layout.master')
@section('judul')
<h1>SELAMAT DATANG !</h1>
@endsection

@section('content')
<h3>Terimakasih telah bergabung di SanberBook. Social Media kita bersama!</h3>
@endsection --}}

@extends('layout.master')

@section('judul')
    <h1>SELAMAT DATANG, {{ $firstName }} {{ $lastName }}!</h1>
@endsection

@section('content')
    <h3>Terima kasih telah bergabung di SanberBook. Social Media kita bersama!</h3>
@endsection
    