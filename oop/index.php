<?php
require_once 'animal.php';
require_once 'Frog.php';
require_once 'Ape.php';

$sheep = new Animal("shaun");
echo "name : " . $sheep->name . "<br>"; // "shaun"
echo "legs : " . $sheep->legs . "<br>"; // 4
echo "cold blooded : " . $sheep->cold_blooded . "<br><br>"; // "no"

$kodok = new Frog("buduk");
echo "name : " . $kodok->name . "<br>"; // "buduk"
echo "legs : " . $kodok->legs . "<br>"; // 4
echo "cold blooded : " . $kodok->cold_blooded . "<br>"; // "no"
echo "jump : "; $kodok->jump(); echo "<br><br>"; // "hop hop"

$sungokong = new Ape("kera sakti");
echo "name : " . $sungokong->name . "<br>"; // "kera sakti"
echo "legs : " . $sungokong->legs . "<br>"; // 2
echo "cold blooded : " . $sungokong->cold_blooded . "<br>"; // "no"
echo "yell : "; $sungokong->yell(); echo "<br>"; // "Auooo"
?>
