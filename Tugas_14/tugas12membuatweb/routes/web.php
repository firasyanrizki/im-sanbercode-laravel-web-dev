<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\daftarController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [HomeController::class, 'utama']);

Route::get('/register', [daftarController::class, 'register']);

Route::post('/welcome', [daftarController::class, 'welcome']);

Route::get('/data-table', function(){
    return view('datatable');
});

Route::get('/table', function(){
    return view('table');
});

// Route::get('/master', function(){
//     return view ('layout.master');
// });