@extends('layout.master')


@section('judul')
<h1>Buat Account Baru!</h1>
@endsection

@section('content')
<h3>Sign Up Form</h3>

<form action="/welcome" method="POST">
    @csrf
    <label>First Name:</label><br>
    <input type="text" name="fname"><br><br>
    <label>Last Name</label><br>
    <input type="text" name="lname"><br><br>
    <label>Gender :</label><br><br>
    <input type="radio">Male<br>
    <input type="radio">Female<br>
    <input type="radio">Other<br><br>

    <label>Nationality :</label><br><br>
    <select country="nationality">
        <option value="Indonesia">Indonesia</option>
        <option value="Singapore">Singapore</option>
        <option value="Malaysia">Malaysia</option>
    </select>
    <br><br>

    <label>Language Spoken :</label><br><br>
    <input type="checkbox" name="language">Indonesia<br>
    <input type="checkbox" name="language">English<br>
    <Input type="checkbox" name="language">Other<br><br>


    <label>Bio :</label><br><br>    
    <textarea name="Bio" rows="10" cols="30"></textarea>
    <br>
    <Input type="submit" value="Sign Up">
@endsection

